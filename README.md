# Robot Bombeiro

Projecto Robot Bombeiro de Laboratório de Sistemas 1, 2017/18

## Requisitos

* Dados dos Robots
* Dados das Provas
* Dados das Equipas
* Dados dos Membros
* Parâmetros do robot
* Estados do Robot
* QtWidgets

## Estrutura APP

* app
	* views
	    * (vários - verificar repositório)
	* controllers
		* database
		* manage_member
		* manage_robot
		* manage_team 
		* manage_match
    * models
    	* robot
		* member
    	* team
    	* match
		* trial
* main.cpp

## Estrutura BD

Tabelas:

* robots
	* id (integer auto-increment)
	* name (varchar)
	* velocity (float)
	* sensors (integer)
	* extinguisher (varchar)
	* detected (integer)
	* total_distance (integer)
	* team_id (integer)
* teams
	* id (integer auto-increment)
	* name (varchar)
	* color (varchar) // #AABBCC
	* robots (integer)
	* members (integer)
* members
	* id (integer auto-increment)
	* name (varchar)
	* number (integer)
	* team_id (integer)
* matches
	* id (integer auto-increment)
	* robot_id (integer)
	* time_start (timestamp)
	* time_finish (timestamp)
* trials
	* id (integer auto-increment)
	* match_id (integer)
	* state (integer)
	* timestamp (timestamp)
* Chaves Primárias e Estrangeiras:
	* robot(id) - Primary Key
	* robot(team_id) - Foreign Key references teams(id)
	* robot(map_id) - Foreign Key references maps(id)
	* teams(id) - Primary Key
	* members(id) - Primary Key
	* members(team_id) - Foreign Key references teams(id)
	* matches(id) - Primary Key
	* matches(robot_id) - FOreign Key references robots(id)
	* trials(id) - Primary Key
	* trials(match_id) - FOreign Key references matches(id)
