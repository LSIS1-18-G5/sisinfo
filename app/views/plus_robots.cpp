#include "plus_robots.h"
#include "ui_plus_robots.h"
#include "create_robots.h"
#include "edit_robots.h"
#include "menumain.h"
#include <QtGui/QStandardItemModel>
#include <QtGui/QStandardItem>
#include <QtCore/QString>
#include <QtWidgets/QMessageBox>

PlusRobots::PlusRobots(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PlusRobots)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    mr = new ManageRobot();

    QPixmap pixBotton1("app/views/img/+.png");
    ui->createRobots->setIcon(pixBotton1);

    QPixmap pixBotton2("app/views/img/reload.png");
    ui->reloadRobots->setIcon(pixBotton2);

    QPixmap pixBotton3("app/views/img/edit.png");
    ui->editRobots->setIcon(pixBotton3);

    QPixmap pixBotton4("app/views/img/minus.png");
    ui->deleteRobots->setIcon(pixBotton4);

    QPixmap pixBotton5("app/views/img/search.png");
    ui->searchRobots->setIcon(pixBotton5);

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);

    populateUI();

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->lineEdit->setPlaceholderText("Nome...");

}

PlusRobots::~PlusRobots()
{
    delete ui;
}

void PlusRobots::populateUI()
{
    QList<Robot> robots = mr->listRobots();
    this->populateTableView(robots);
}

void PlusRobots::populateTableView(QList<Robot> robots)
{
    QStandardItemModel *pmodel = new QStandardItemModel(this);

    for(int i = 0; i<robots.count(); i++)
    {
        pmodel->setItem(i,0,new QStandardItem(QString::number(robots[i].getUID())));
        pmodel->setItem(i,1,new QStandardItem(QString::fromStdString(robots[i].getName())));
        pmodel->setItem(i,2,new QStandardItem(QString::number(robots[i].getVelocity())));
        pmodel->setItem(i,3,new QStandardItem(QString::number(robots[i].getSensors())));
        pmodel->setItem(i,4,new QStandardItem(QString::fromStdString(robots[i].getExtinguisher())));
        pmodel->setItem(i,5,new QStandardItem(QString::number(robots[i].getDetected())));
        pmodel->setItem(i,6,new QStandardItem(QString::number(robots[i].getTotalDistance())));
        pmodel->setItem(i,7,new QStandardItem(QString::fromStdString(mr->getTeamById(robots[i].getTeam()))));
    }

    pmodel->setHeaderData(0, Qt::Horizontal, QObject::tr("UID"));
    pmodel->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    pmodel->setHeaderData(2, Qt::Horizontal, QObject::tr("Velocidade"));
    pmodel->setHeaderData(3, Qt::Horizontal, QObject::tr("Sensores"));
    pmodel->setHeaderData(4, Qt::Horizontal, QObject::tr("Extintor"));
    pmodel->setHeaderData(5, Qt::Horizontal, QObject::tr("Detetados"));
    pmodel->setHeaderData(6, Qt::Horizontal, QObject::tr("Distância"));
    pmodel->setHeaderData(7, Qt::Horizontal, QObject::tr("Equipa"));

    ui->tableView->setModel( pmodel );
}

void PlusRobots::on_searchRobots_clicked()
{
    QList<Robot> robots = mr->searchRobots(ui->lineEdit->text().toStdString());
    populateTableView(robots);
}

void PlusRobots::on_createRobots_clicked()
{
    CreateRobots *create = new CreateRobots(this);
    create->setCon(mr);
    create->show();
}

void PlusRobots::on_editRobots_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        Robot r;
        r.setUID(index.sibling(row, 0).data().toInt());
        r.setName(index.sibling(row, 1).data().toString().toStdString());
        r.setVelocity(index.sibling(row, 2).data().toFloat());
        r.setSensors(index.sibling(row, 3).data().toInt());
        r.setExtinguisher(index.sibling(row, 4).data().toString().toStdString());
        r.setDetected(index.sibling(row, 5).data().toInt());
        r.setTotalDistance(index.sibling(row, 6).data().toInt());
        r.setTeam(mr->getTeamByName(index.sibling(row, 7).data().toString().toStdString()));
        EditRobots *edit = new EditRobots(this);
        edit->setCon(mr, r);
        edit->show();
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar o robot que pretende atualizar.");
    }
}

void PlusRobots::on_deleteRobots_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        int reply = QMessageBox::question(this,"Eliminar Robot", "Pretende eliminar o robot " + index.sibling(row, 1).data().toString() + "?\n Aviso: Esta ação é irreversivel!", QMessageBox::Yes, QMessageBox::No);
        if(reply == QMessageBox::Yes){
            mr->removeFromTeam(mr->getTeamByName(index.sibling(row, 7).data().toString().toStdString()));
            mr->deleteRobot(index.sibling(row, 0).data().toInt());
            this->populateUI();
        }
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar o robot que pretende eliminar.");
    }
}

void PlusRobots::on_reloadRobots_clicked()
{
    this->populateUI();
}

void PlusRobots::on_back_clicked()
{
    delete mr;
    this->close();
    MenuMain * menu = new MenuMain(this);
    menu->show();
}
