#ifndef PLUSROBOTS_H
#define PLUSROBOTS_H

#include <QtWidgets/QMainWindow>
#include <QtCore/QList>
#include "../controllers/manage_robot.h"

namespace Ui 
{
    class PlusRobots;
}

class PlusRobots : public QMainWindow
{
    Q_OBJECT

    public:
    explicit PlusRobots(QWidget *parent = 0);
    ~PlusRobots();

    private slots:
    void on_searchRobots_clicked();
    void on_createRobots_clicked();
    void on_editRobots_clicked();
    void on_reloadRobots_clicked();
    void on_deleteRobots_clicked();
    void on_back_clicked();
    void populateUI();

    private:
    Ui::PlusRobots *ui;
    ManageRobot *mr;
    void populateTableView(QList<Robot>);
};

#endif // PLUSROBOTS_H
