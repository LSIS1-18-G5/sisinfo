#include "menumain.h"
#include "ui_menumain.h"
#include "plus_matches.h"
#include "plus_members.h"
#include "plus_robots.h"
#include "plus_teams.h"
#include "start_matches.h"
#include <QtWidgets/QMessageBox>

MenuMain::MenuMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MenuMain)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    mc = new MenuChecks();

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->labelRobot->setPixmap(pix);

    QPixmap pixBotton1("app/views/img/robot.png");
    ui->plusRobots->setIcon(pixBotton1);
    ui->plusRobots->setIconSize(QSize(150,150));

    QPixmap pixBotton2("app/views/img/user.png");
    ui->plusMembers->setIcon(pixBotton2);
    ui->plusMembers->setIconSize(QSize(100,100));

    QPixmap pixBotton3("app/views/img/team.png");
    ui->plusTeams->setIcon(pixBotton3);
    ui->plusTeams->setIconSize(QSize(150,150));

    QPixmap pixBotton4("app/views/img/podium.png");
    ui->plusMatches->setIcon(pixBotton4);
    ui->plusMatches->setIconSize(QSize(80,80));
}

MenuMain::~MenuMain()
{
    delete mc;
    delete ui;
}

void MenuMain::on_plusRobots_clicked()
{
    if(mc->checkElement("teams") > 0) {
        this->close();
        delete mc;
        PlusRobots *robots = new PlusRobots(this);
        robots->show();
    } else {
        QMessageBox::critical(this, "Erro", "Tem de criar pelo menos 1 equipa!");
    }   
}

void MenuMain::on_plusMembers_clicked()
{
    if(mc->checkElement("teams") > 0) {
        this->close();
        delete mc;
        PlusMembers *members= new PlusMembers(this);
        members->show();
    } else {
        QMessageBox::critical(this, "Erro", "Tem de criar pelo menos 1 equipa!");
    }      
}

void MenuMain::on_plusTeams_clicked()
{
    this->close();
    delete mc;
    PlusTeams *teams = new PlusTeams(this);
    teams->show();
}

void MenuMain::on_plusMatches_clicked()
{
    if(mc->checkElement("robots") > 0) {
        this->close();
        delete mc;
        PlusMatches *matches = new PlusMatches(this);
        matches->show();
    } else {
        QMessageBox::critical(this, "Erro", "Tem de criar pelo menos 1 robot!");
    }
}

void MenuMain::on_EXIT_clicked()
{
    this->close();
    delete mc;
}

