#include "plus_members.h"
#include "ui_plus_members.h"
#include "create_members.h"
#include "edit_members.h"
#include "menumain.h"
#include <QtGui/QStandardItemModel>
#include <QtGui/QStandardItem>
#include <QtCore/QString>
#include <QtWidgets/QMessageBox>

PlusMembers::PlusMembers(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PlusMembers)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    mm = new ManageMember();

    QPixmap pixBotton1("app/views/img/+.png");
    ui->createMembers->setIcon(pixBotton1);

    QPixmap pixBotton2("app/views/img/reload.png");
    ui->reloadMembers->setIcon(pixBotton2);

    QPixmap pixBotton3("app/views/img/edit.png");
    ui->editMembers->setIcon(pixBotton3);

    QPixmap pixBotton4("app/views/img/minus.png");
    ui->deleteMembers->setIcon(pixBotton4);

    QPixmap pixBotton5("app/views/img/search.png");
    ui->searchMembers->setIcon(pixBotton5);

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);
    
    populateUI();

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->lineEdit->setPlaceholderText("Nome...");
}

PlusMembers::~PlusMembers()
{
    delete mm;
    delete ui;
}

void PlusMembers::populateUI()
{
    QList<Member> members = mm->listMembers();
    this->populateTableView(members);
}

void PlusMembers::populateTableView(QList<Member> members)
{
    QStandardItemModel *pmodel = new QStandardItemModel(this);

    for(int i = 0; i<members.count(); i++)
    {
        pmodel->setItem(i,0,new QStandardItem(QString::number(members[i].getUID())));
        pmodel->setItem(i,1,new QStandardItem(QString::fromStdString(members[i].getName())));
        pmodel->setItem(i,2,new QStandardItem(QString::number(members[i].getNumber())));
        pmodel->setItem(i,3,new QStandardItem(QString::fromStdString(mm->getTeamById(members[i].getTeam()))));
    }

    pmodel->setHeaderData(0, Qt::Horizontal, QObject::tr("UID"));
    pmodel->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    pmodel->setHeaderData(2, Qt::Horizontal, QObject::tr("Número"));
    pmodel->setHeaderData(3, Qt::Horizontal, QObject::tr("Equipa"));

    ui->tableView->setModel( pmodel );
}

void PlusMembers::on_searchMembers_clicked()
{
    QList<Member> members = mm->searchMembers(ui->lineEdit->text().toStdString());
    populateTableView(members);
}

void PlusMembers::on_createMembers_clicked()
{
    CreateMembers *create = new CreateMembers(this);
    create->setCon(mm);
    create->show();
}

void PlusMembers::on_editMembers_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        Member m;
        m.setUID(index.sibling(row, 0).data().toInt());
        m.setName(index.sibling(row, 1).data().toString().toStdString());
        m.setNumber(index.sibling(row, 2).data().toInt());
        m.setTeam(mm->getTeamByName(index.sibling(row, 3).data().toString().toStdString()));
        EditMembers *edit = new EditMembers(this);
        edit->setCon(mm, m);
        edit->show();
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar o membro que pretende atualizar.");
    }
}

void PlusMembers::on_deleteMembers_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        int reply = QMessageBox::question(this,"Eliminar Membro", "Pretende eliminar o membro " + index.sibling(row, 1).data().toString() + "?\n Aviso: Esta ação é irreversivel!", QMessageBox::Yes, QMessageBox::No);
        if(reply == QMessageBox::Yes){
            mm->removeFromTeam(mm->getTeamByName(index.sibling(row, 3).data().toString().toStdString()));
            mm->deleteMember(index.sibling(row, 0).data().toInt());
            this->populateUI();
        }
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar o membro que pretende eliminar.");
    }
}

void PlusMembers::on_reloadMembers_clicked()
{
    this->populateUI();
}

void PlusMembers::on_backButton_clicked()
{
    delete mm;
    this->close();
    MenuMain *criar = new MenuMain(this);
    criar->show();
}
