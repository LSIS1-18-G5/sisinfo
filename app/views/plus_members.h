#ifndef PLUSMEMBERS_H
#define PLUSMEMBERS_H

#include <QtWidgets/QMainWindow>
#include <QtCore/QList>
#include "../controllers/manage_member.h"

namespace Ui 
{
    class PlusMembers;
}

class PlusMembers : public QMainWindow
{
    Q_OBJECT

    public:
    explicit PlusMembers(QWidget *parent = 0);
    ~PlusMembers();

    private slots:
    void on_searchMembers_clicked();
    void on_createMembers_clicked();
    void on_editMembers_clicked();
    void on_reloadMembers_clicked();
    void on_deleteMembers_clicked();
    void on_backButton_clicked();
    void populateUI();

    private:
    Ui::PlusMembers *ui;
    ManageMember *mm;
    void populateTableView(QList<Member>);

};

#endif // PLUSMEMBERS_H
