#include "create_members.h"
#include "ui_create_members.h"
#include "menumain.h"
#include "iostream"
#include "../controllers/manage_team.h"
#include <QtWidgets/QMessageBox>

CreateMembers::CreateMembers(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CreateMembers)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);
    ManageTeam *mt = new ManageTeam();
    ui->team->addItems(mt->getTeams());
    delete mt;

    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void CreateMembers::setCon(ManageMember *mm)
{
    this->mm = mm;
}

CreateMembers::~CreateMembers()
{
    delete mm;
    delete ui;
}

void CreateMembers::on_back_buttom_clicked()
{
    this->close();
}

void CreateMembers::on_inserir_clicked()
{
    if(ui->inserirNome->text().isEmpty() || ui->inserirNumero->text().isEmpty() || ui->team->currentText().isEmpty()){
        QMessageBox::warning(this,"Aviso","Todos os campos têm preenchimento obrigatório.");
    } else {
        Member m;
        m.setName(ui->inserirNome->text().toStdString());
        m.setNumber((int)ui->inserirNumero->text().toInt());
        m.setTeam((mm)->getTeamByName(ui->team->currentText().toStdString()));

        int result = mm->createMember(m);
        if(result == 1) {
            QMessageBox::information(this,"Sucesso","Membro inserido com sucesso!");
            std::cout << "Membro inserido com sucesso!" <<std::endl;
            emit finished(1);
            this->close();
        } else {
            std::cout << "Erro ao inserir!" << std::endl;
        }
    }
}
