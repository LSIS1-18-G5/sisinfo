#ifndef PLUSMATCHES_H
#define PLUSMATCHES_H

#include <QtWidgets/QMainWindow>
#include <QtCore/QList>
#include "../controllers/manage_match.h"

namespace Ui 
{
    class PlusMatches;
}

class PlusMatches : public QMainWindow
{
    Q_OBJECT

    public:
    explicit PlusMatches(QWidget *parent = 0);
    ~PlusMatches();

    private slots:
    void on_searchMatches_clicked();
    void on_startMatches_clicked();
    void on_viewMatches_clicked();
    void on_reloadMatches_clicked();
    void on_deleteMatches_clicked();
    void on_backButton_clicked();
    void populateUI();
    void startTrial(int);

    private:
    Ui::PlusMatches *ui;
    ManageMatch *mm;
    void populateTableView(QList<Match>);

};

#endif // PLUSMATCHES_H
