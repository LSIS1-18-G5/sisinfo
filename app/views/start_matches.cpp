#include "start_matches.h"
#include "ui_start_matches.h"
//#include "trial_matches.h"
#include "menumain.h"
StartMatches::StartMatches(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StartMatches)
{
    ui->setupUi(this);

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_3->setPixmap(pix);

    ManageMatch *mm2 = new ManageMatch();
    ui->robots->addItems(mm2->getRobots());
    delete mm2;

    connect(this, SIGNAL( start(int) ), parent, SLOT( startTrial(int) ) );

}

StartMatches::~StartMatches()
{
    delete mm;
    delete ui;
}

void StartMatches::setCon(ManageMatch* mm)
{
    this->mm = mm;
}

void StartMatches::on_startMatches_clicked()
{
    int robot = this->mm->getRobotByName(ui->robots->currentText().toStdString());
    //mm->startTrial(robot);
    this->close();
    emit start(robot);
    /* TrialMatches *start = new TrialMatches(this);
    start->props(mm, robot);
    start->show(); */

}

void StartMatches::on_backButton_clicked()
{
    this->close();
}
