#ifndef CREATEMEMBERS_H
#define CREATEMEMBERS_H

#include <QtWidgets/QMainWindow>
#include "plus_members.h"

namespace Ui 
{
    class CreateMembers;
}

class CreateMembers : public QMainWindow
{
    Q_OBJECT

    public:
    explicit CreateMembers(QWidget *parent = 0);
    void setCon(ManageMember*);
    ~CreateMembers();

    private slots:
    void on_back_buttom_clicked();
    void on_inserir_clicked();

    private:
    Ui::CreateMembers *ui;
    ManageMember* mm;

    signals:
    void finished(int);
};

#endif // CREATEMEMBERS_H
