#ifndef EDITROBOTS_H
#define EDITROBOTS_H

#include <QtWidgets/QMainWindow>
#include "plus_robots.h"

namespace Ui 
{
    class EditRobots;
}

class EditRobots : public QMainWindow
{
    Q_OBJECT

    public:
    explicit EditRobots(QWidget *parent = 0);
    void setCon(ManageRobot*, Robot&);
    ~EditRobots();
 

    private slots:   
    void on_updateRobot_clicked();
    void on_backButton_clicked();

    private:
    Ui::EditRobots *ui;
    ManageRobot *mr;
    Robot r;

    signals:
    void finished(int);
};

#endif // EDITROBOTS_H
