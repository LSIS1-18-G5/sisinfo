#ifndef STARTMATCHES_H
#define STARTMATCHES_H

#include <QtWidgets/QMainWindow>
#include "plus_matches.h"

namespace Ui 
{
    class StartMatches;
}

class StartMatches : public QMainWindow
{
    Q_OBJECT

    public:
    explicit StartMatches(QWidget *parent = 0);
    ~StartMatches();

    void setCon(ManageMatch*);

    private slots:
    void on_backButton_clicked();
    void on_startMatches_clicked();

    private:
    Ui::StartMatches *ui;
    ManageMatch *mm;

    signals:
    void start(int);
};

#endif // STARTMATCHES_H
