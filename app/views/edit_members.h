#ifndef EDITMEMBERS_H
#define EDITMEMBERS_H

#include <QtWidgets/QMainWindow>
#include "plus_members.h"

namespace Ui 
{
    class EditMembers;
}

class EditMembers : public QMainWindow
{
    Q_OBJECT

    public:
    explicit EditMembers(QWidget *parent = 0);
    void setCon(ManageMember*, Member&);
    ~EditMembers();
 

    private slots:   
    void on_updateMember_clicked();
    void on_backButton_clicked();

    private:
    Ui::EditMembers *ui;
    ManageMember *mm;
    Member m;

    signals:
    void finished(int);

};

#endif // EDITMEMBERS_H
