#include "trial_matches.h"
#include "ui_trial_matches.h"
#include <QtCore/QString>
#include <QtGui/QStandardItemModel>
#include <sstream>
TrialMatches::TrialMatches(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TrialMatches)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));
    
    QPixmap pix("app/views/img/match_wallpaper.png");
    ui->label_10->setPixmap(pix);

    QPixmap arrow_1("app/views/img/arrowUp_b.png");
    ui->arrowUp->setPixmap(arrow_1);

    QPixmap arrow_2("app/views/img/arrowDown_b.png");
    ui->arrowDown->setPixmap(arrow_2);

    QPixmap arrow_3("app/views/img/arrowLeft_b.png");
    ui->arrowLeft->setPixmap(arrow_3);

    QPixmap arrow_4("app/views/img/arrowRight_b.png");
    ui->arrowRight->setPixmap(arrow_4);

    QPixmap fire("app/views/img/fire_b.png");
    ui->fire->setPixmap(fire);

    serial= new QSerialPort(this);
    serial->setPortName("/dev/ttyUSB0");
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadOnly);

    connect(serial, SIGNAL(readyRead()), this, SLOT(serialReceived()));
    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void TrialMatches::serialReceived()
{
    QByteArray value;
    value = serial->readAll();
    string v = value.toStdString();
    ui->number->setText(QString::fromStdString(v));
    changeStateOld();
    changeStateNew(trim(v));
    stringstream number;
    number << v;
    int n = 0;
    number>>n;
    mm->saveTrial(mid,n);
    if(n == 5) mm->fireDetected(rid);
}

string TrialMatches::trim(const string& str)
{
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first)
    {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

void TrialMatches::changeStateOld()
{
    QPixmap arrow_1("app/views/img/arrowUp_b.png");
    ui->arrowUp->setPixmap(arrow_1);

    QPixmap arrow_2("app/views/img/arrowDown_b.png");
    ui->arrowDown->setPixmap(arrow_2);

    QPixmap arrow_3("app/views/img/arrowLeft_b.png");
    ui->arrowLeft->setPixmap(arrow_3);

    QPixmap arrow_4("app/views/img/arrowRight_b.png");
    ui->arrowRight->setPixmap(arrow_4);

    QPixmap fire("app/views/img/fire_b.png");
    ui->fire->setPixmap(fire);
}

void TrialMatches::changeStateNew(string v)
{
    if (v == "1") {
        ui->arrowUp->setPixmap(QPixmap("app/views/img/arrowUp.png"));
        ui->arrowUp->setScaledContents(true);
    } else if (v == "2") {
        ui->arrowRight->setPixmap(QPixmap ("app/views/img/arrowRight.png"));
        ui->arrowRight->setScaledContents(true);
    } else if (v == "3") {
        ui->arrowLeft->setPixmap(QPixmap ("app/views/img/arrowLeft.png"));
        ui->arrowLeft->setScaledContents(true);
    } else if(v == "5") {
        ui->fire->setPixmap(QPixmap ("app/views/img/fire.png"));
        ui->fire->setScaledContents(true);
    }
}

TrialMatches::~TrialMatches()
{
    delete serial;
    delete mm;
    delete ui;
}

void TrialMatches::props(ManageMatch *mm, int rid)
{
    this->mm = mm;
    this->rid = rid;
    this->mm->startTrial(rid);
    this->mid = this->mm->getLastId();

    ui->label_2->setText("Prova: " + QString::fromStdString(this->mm->getRobotById(this->rid)));
}

void TrialMatches::on_stopTrial_clicked()
{
    mm->stopTrial(mid);
    disconnect(serial, SIGNAL(readyRead()), 0,0);
    emit finished(1);
    serial->close();
    this->close();
}
