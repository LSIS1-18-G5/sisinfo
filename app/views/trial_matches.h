#ifndef TRIALMATCHES_H
#define TRIALMATCHES_H

#include <QtWidgets/QMainWindow>
#include <QtCore/QList>
#include <QtSerialPort/QSerialPort>
#include <QtCore/QByteArray>
#include <QtCore/QDebug>

#include "plus_matches.h"

namespace Ui 
{
    class TrialMatches;
}

class TrialMatches : public QMainWindow
{
    Q_OBJECT

    public:
    explicit TrialMatches(QWidget *parent = 0);
    ~TrialMatches();

    void props(ManageMatch*, int);

    private slots:
    void on_stopTrial_clicked();
    void serialReceived();
    string trim(const string&);
    void changeStateOld();
    void changeStateNew(string);

    private:
    QSerialPort *serial;
    Ui::TrialMatches *ui;
    ManageMatch *mm;
    int rid;
    int mid;

    signals:
    void finished(int);
};

#endif // TRIALMATCHES_H
