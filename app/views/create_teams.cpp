#include "create_teams.h"
#include "ui_create_teams.h"
#include "menumain.h"
#include "iostream"
#include <QtWidgets/QMessageBox>

CreateTeams::CreateTeams(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CreateTeams)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));
    
    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);

    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void CreateTeams::setCon(ManageTeam *mt)
{
    this->mt = mt;
}

CreateTeams::~CreateTeams()
{
    delete mt;
    delete ui;
}

void CreateTeams::on_back_button_clicked()
{
    this->close();
}

void CreateTeams::on_inserir_clicked()
{
    if(ui->name->text().isEmpty() || ui->color->text().isEmpty()){
        QMessageBox::warning(this,"Aviso","Todos os campos têm preenchimento obrigatório.");
    } else {
        Team t;
        t.setName(ui->name->text().toStdString());
        t.setColor(ui->color->text().toStdString());

        int result = mt->createTeam(t);
        if(result == 1){
            QMessageBox::information(this,"Sucesso","Equipa inserida com sucesso!");
            std::cout << "Equipa inserida com sucesso!" <<std::endl;
            emit finished(1);
            this->close();
        } else {
            std::cout << "Erro ao inserir!" << std::endl;
        }
    }
}
