#ifndef VIEWTEAMS_H
#define VIEWTEAMS_H

#include <QtWidgets/QMainWindow>
#include "plus_matches.h"

namespace Ui
{
    class ViewMatches;
}

class ViewMatches : public QMainWindow
{
    Q_OBJECT

    public:
    explicit ViewMatches(QWidget *parent = 0);
    ~ViewMatches();

    void setCon(ManageMatch*, Match&);
    void populateView();

    private slots:
    void on_backButton_clicked();

    private:
    Ui::ViewMatches *ui;
    ManageMatch *mm;
    Match m;
};

#endif // VIEWTEAMS_H
