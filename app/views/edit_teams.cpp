#include "edit_teams.h"
#include "ui_edit_teams.h"
#include "menumain.h"
#include <QtWidgets/QMessageBox>

EditTeams::EditTeams(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditTeams)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));
    
    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);

    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void EditTeams::setCon(ManageTeam* mt, Team &t)
{
    this->mt = mt;
    this->t = t;
    
    ui->lineEdit_2->setText(QString::fromStdString(t.getName()));
    ui->lineEdit->setText(QString::fromStdString(t.getColor()));
}

EditTeams::~EditTeams()
{
    delete mt;
    delete ui;
}

void EditTeams::on_updateTeam_clicked()
{
    if(ui->lineEdit->text().isEmpty() || ui->lineEdit_2->text().isEmpty()){
        QMessageBox::warning(this,"Aviso","Todos os campos têm preenchimento obrigatório.");
    } else {
        t.setName(ui->lineEdit_2->text().toStdString());
        t.setColor(ui->lineEdit->text().toStdString());

        int result = mt->updateTeam(t);
        if(result == 1){
            QMessageBox::information(this,"Sucesso","Equipa atualizada com sucesso!");
            std::cout << "Equipa atualizada com sucesso!" <<std::endl;
            emit finished(1);
            this->close();
        } else {
            QMessageBox::information(this,"Erro","Não foi possivel atualizar a equipa!\nContacte o administrador.");
            std::cout << "Erro ao atualizar!" << std::endl;
        }
    }
}

void EditTeams::on_back_button_clicked()
{
    this->close();
}
