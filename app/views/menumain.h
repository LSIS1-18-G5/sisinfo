#ifndef MENUMAIN_H
#define MENUMAIN_H

#include <QtWidgets/QMainWindow>
#include "../controllers/menu_checks.h"

namespace Ui 
{
    class MenuMain;
}

class MenuMain : public QMainWindow
{
    Q_OBJECT

    public:
    explicit MenuMain(QWidget *parent = 0);
    ~MenuMain();

    private slots:
    void on_plusRobots_clicked();
    void on_plusMembers_clicked();
    void on_plusTeams_clicked();
    void on_plusMatches_clicked();
    void on_EXIT_clicked();

    private:
    Ui::MenuMain *ui;
    MenuChecks *mc;
};

#endif // MENUMAIN_H
