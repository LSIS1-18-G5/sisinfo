#include "edit_robots.h"
#include "ui_edit_robots.h"
#include "menumain.h"
#include "../controllers/manage_team.h"
#include <QtWidgets/QMessageBox>

EditRobots::EditRobots(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditRobots)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));
    
    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);
    ManageTeam *mt = new ManageTeam();
    ui->team->addItems(mt->getTeams());
    delete mt;

    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void EditRobots::setCon(ManageRobot* mr, Robot &r)
{
    this->mr = mr;
    this->r = r;
    
    ui->name->setText(QString::fromStdString(r.getName()));
    ui->velocity->setText(QString::number(r.getVelocity()));
    ui->sensors->setText(QString::number(r.getSensors()));
    ui->extinguisher->setText(QString::fromStdString(r.getExtinguisher()));
    ui->detected->setText(QString::number(r.getDetected()));
    ui->distance->setText(QString::number(r.getTotalDistance()));
    ui->team->setCurrentIndex(ui->team->findText(QString::fromStdString(mr->getTeamById(this->r.getTeam()))));
}

EditRobots::~EditRobots()
{
    delete ui;
}

void EditRobots::on_updateRobot_clicked()
{
    if(ui->name->text().isEmpty() || ui->velocity->text().isEmpty() || ui->team->currentText().isEmpty() ||
    ui->sensors->text().isEmpty() || ui->extinguisher->text().isEmpty() || ui->detected->text().isEmpty() ||
    ui->distance->text().isEmpty()){
        QMessageBox::warning(this,"Aviso","Todos os campos têm preenchimento obrigatório.");
    } else {
        r.setName(ui->name->text().toStdString());
        r.setVelocity(ui->velocity->text().toDouble());
        r.setSensors(ui->sensors->text().toInt());
        r.setExtinguisher(ui->extinguisher->text().toStdString());
        r.setDetected(ui->detected->text().toInt());
        r.setTotalDistance(ui->distance->text().toInt());
        int old_team = r.getTeam();
        r.setTeam((mr)->getTeamByName(ui->team->currentText().toStdString()));

        int result = mr->updateRobot(r, old_team);
        if(result == 1){
            QMessageBox::information(this,"Sucesso","Robot atualizado com sucesso!");
            std::cout << "Robot atualizado com sucesso!" <<std::endl;
            emit finished(1);
            this->close();
        } else {
            QMessageBox::information(this,"Erro","Não foi possivel atualizar o membro!\nContacte o administrador.");
            std::cout << "Erro ao atualizar!" << std::endl;
        }
    }
}

void EditRobots::on_backButton_clicked()
{
    this->close();
}
