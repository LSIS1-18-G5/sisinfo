#include "plus_teams.h"
#include "ui_plus_teams.h"
#include "create_teams.h"
#include "edit_teams.h"
#include "menumain.h"
#include <QtGui/QStandardItemModel>
#include <QtGui/QStandardItem>
#include <QtCore/QString>
#include <QtWidgets/QMessageBox>

PlusTeams::PlusTeams(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PlusTeams)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));
    
    mt = new ManageTeam();

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);

    QPixmap pixBotton1("app/views/img/+.png");
    ui->createTeams->setIcon(pixBotton1);

    QPixmap pixBotton2("app/views/img/reload.png");
    ui->reloadTeams->setIcon(pixBotton2);

    QPixmap pixBotton3("app/views/img/edit.png");
    ui->editTeams->setIcon(pixBotton3);

    QPixmap pixBotton4("app/views/img/minus.png");
    ui->deleteTeams->setIcon(pixBotton4);

    QPixmap pixBotton5("app/views/img/search.png");
    ui->searchTeams->setIcon(pixBotton5);

    populateUI();

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->lineEdit->setPlaceholderText("Nome...");
}

PlusTeams::~PlusTeams()
{
    delete mt;
    delete ui;
}

void PlusTeams::populateUI()
{
    QList<Team> teams = mt->listTeams();
    this->populateTableView(teams);
}

void PlusTeams::populateTableView(QList<Team> teams)
{
    QStandardItemModel *pmodel = new QStandardItemModel(this);

    for(int i = 0; i<teams.count(); i++)
    {
        pmodel->setItem(i,0,new QStandardItem(QString::number(teams[i].getUID())));
        pmodel->setItem(i,1,new QStandardItem(QString::fromStdString(teams[i].getName())));
        pmodel->setItem(i,2,new QStandardItem(QString::fromStdString(teams[i].getColor())));
        pmodel->setItem(i,3,new QStandardItem(QString::number(teams[i].getRobots())));
        pmodel->setItem(i,4,new QStandardItem(QString::number(teams[i].getMembers())));
    }

    pmodel->setHeaderData(0, Qt::Horizontal, QObject::tr("UID"));
    pmodel->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    pmodel->setHeaderData(2, Qt::Horizontal, QObject::tr("Color"));
    pmodel->setHeaderData(3, Qt::Horizontal, QObject::tr("Robots"));
    pmodel->setHeaderData(4, Qt::Horizontal, QObject::tr("Membros"));

    ui->tableView->setModel( pmodel );
}

void PlusTeams::on_searchTeams_clicked()
{
    QList<Team> teams = mt->searchTeams(ui->lineEdit->text().toStdString());
    populateTableView(teams);
}

void PlusTeams::on_createTeams_clicked()
{
    CreateTeams *create = new CreateTeams(this);
    create->setCon(mt);
    create->show();
}

void PlusTeams::on_reloadTeams_clicked()
{
    this->populateUI();
}

void PlusTeams::on_deleteTeams_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        int reply = QMessageBox::question(this,"Eliminar Equipa", "Ao eliminar uma equipa, os membros e robots associados também serão eliminados. Pretende eliminar a equipa " + index.sibling(row, 1).data().toString() + "?\n Aviso: Esta ação é irreversivel!", QMessageBox::Yes, QMessageBox::No);
        if(reply == QMessageBox::Yes){
            mt->deleteTeam(index.sibling(row, 0).data().toInt());
            this->populateUI();
        }
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar a equipa que pretende atualizar.");
    }
}

void PlusTeams::on_editTeams_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        Team t;
        t.setUID(index.sibling(row, 0).data().toInt());
        t.setName(index.sibling(row, 1).data().toString().toStdString());
        t.setColor(index.sibling(row, 2).data().toString().toStdString());
        t.setRobots(index.sibling(row, 3).data().toInt());
        t.setMembers(index.sibling(row, 4).data().toInt());
        EditTeams *edit = new EditTeams(this);
        edit->setCon(mt, t);
        edit->show();
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar a equipa que pretende eliminar.");
    }
}

void PlusTeams::on_backButton_clicked()
{
    delete mt;
    this->close();
    MenuMain *criar = new MenuMain(this);
    criar->show();
}
