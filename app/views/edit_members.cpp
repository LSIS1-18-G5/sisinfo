#include "edit_members.h"
#include "ui_edit_members.h"
#include "menumain.h"
#include "../controllers/manage_team.h"
#include <QtWidgets/QMessageBox>

EditMembers::EditMembers(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditMembers)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);
    ManageTeam *mt = new ManageTeam();
    ui->comboBox_4->addItems(mt->getTeams());
    delete mt;

    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void EditMembers::setCon(ManageMember* mm, Member &m)
{
    this->mm = mm;
    this->m = m;
    
    ui->lineEdit_3->setText(QString::fromStdString(m.getName()));
    ui->lineEdit_4->setText(QString::number(m.getNumber()));
    ui->comboBox_4->setCurrentIndex(ui->comboBox_4->findText(QString::fromStdString(mm->getTeamById(this->m.getTeam()))));
}

EditMembers::~EditMembers()
{
    delete mm;
    delete ui;
}

void EditMembers::on_updateMember_clicked()
{
    if(ui->lineEdit_3->text().isEmpty() || ui->lineEdit_4->text().isEmpty() || ui->comboBox_4->currentText().isEmpty()){
        QMessageBox::warning(this,"Aviso","Todos os campos têm preenchimento obrigatório.");
    } else {
        m.setName(ui->lineEdit_3->text().toStdString());
        m.setNumber((int)ui->lineEdit_4->text().toInt());
        int old_team = m.getTeam();
        m.setTeam((mm)->getTeamByName(ui->comboBox_4->currentText().toStdString()));

        int result = mm->updateMember(m, old_team);
        if(result == 1){
            QMessageBox::information(this,"Sucesso","Membro atualizado com sucesso!");
            std::cout << "Membro atualizado com sucesso!" <<std::endl;
            emit finished(1);
            this->close();
        } else {
            QMessageBox::information(this,"Erro","Não foi possivel atualizar o membro!\nContacte o administrador.");
            std::cout << "Erro ao atualizar!" << std::endl;
        }
    }
}

void EditMembers::on_backButton_clicked()
{
    this->close();
}
