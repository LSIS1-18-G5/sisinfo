#include "view_matches.h"
#include "ui_view_matches.h"
#include <QStandardItemModel>

ViewMatches::ViewMatches(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewMatches)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));
    
    QPixmap pix("app/views/img/match_wallpaper.png");
    ui->label_10->setPixmap(pix);

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void ViewMatches::setCon(ManageMatch *mm, Match &m)
{
    this->mm = mm;
    this->m = m;

    populateView();
}

void ViewMatches::populateView()
{
    ui->match->setText("Prova #" + QString::number(m.getUID()));
    ui->robot->setText(QString::fromStdString(mm->getRobotById(m.getRobot())));
    ui->equipa->setText(QString::fromStdString(mm->getRobotTeam(m.getRobot())));
    ui->start->setText(QString::fromStdString(m.getTimeStart()));
    ui->finish->setText(QString::fromStdString(m.getTimeFinish()));

    QStandardItemModel *pmodel = new QStandardItemModel(this);
    QList<Trial> trials = mm->getTrials(m.getUID());

    for(int i = 0; i<trials.count(); i++)
    {
        pmodel->setItem(i,0,new QStandardItem(QString::number(trials[i].getState())));
        pmodel->setItem(i,1,new QStandardItem(QString::fromStdString(trials[i].getTimestamp())));
     }

    pmodel->setHeaderData(0, Qt::Horizontal, QObject::tr("Estado"));
    pmodel->setHeaderData(1, Qt::Horizontal, QObject::tr("Tempo"));

    ui->tableView->setModel( pmodel );
}

ViewMatches::~ViewMatches()
{
    delete mm;
    delete ui;
}

void ViewMatches::on_backButton_clicked()
{
    this->close();
}
