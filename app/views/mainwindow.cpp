#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "menumain.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->labelRobot->setPixmap(pix);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_iniciar_clicked()
{
   this->close();
   MenuMain *menu= new MenuMain (this);
   menu->show();
}
