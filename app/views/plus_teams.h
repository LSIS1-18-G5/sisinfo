#ifndef PLUSTEAM_H
#define PLUSTEAM_H

#include <QtWidgets/QMainWindow>
#include <QtCore/QList>
#include "../controllers/manage_team.h"

namespace Ui 
{
    class PlusTeams;
}

class PlusTeams : public QMainWindow
{
    Q_OBJECT

    public:
    explicit PlusTeams(QWidget *parent = 0);
    ~PlusTeams();

    private slots:
    void on_searchTeams_clicked();
    void on_createTeams_clicked();
    void on_editTeams_clicked();
    void on_reloadTeams_clicked();
    void on_deleteTeams_clicked();
    void on_backButton_clicked();
    void populateUI();

private:
    Ui::PlusTeams *ui;
    ManageTeam *mt;
    void populateTableView(QList<Team>);
};

#endif // PLUSTEAM_H
