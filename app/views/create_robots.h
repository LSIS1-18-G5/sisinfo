#ifndef CREATEROBOTS_H
#define CREATEROBOTS_H

#include <QtWidgets/QMainWindow>
#include "plus_robots.h"


namespace Ui 
{
    class CreateRobots;
}

class CreateRobots : public QMainWindow
{
    Q_OBJECT

    public:
    explicit CreateRobots(QWidget *parent = 0);
    void setCon(ManageRobot*);
    ~CreateRobots();

    private slots:
    void on_back_clicked();
    void on_inserir_clicked();

    private:
    Ui::CreateRobots *ui;
    ManageRobot *mr;

    signals:
    void finished(int);
};

#endif // CREATEROBOTS_H
