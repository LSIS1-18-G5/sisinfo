#ifndef EDITTEAMS_H
#define EDITTEAMS_H

#include <QtWidgets/QMainWindow>
#include "plus_teams.h"

namespace Ui 
{
    class EditTeams;
}

class EditTeams : public QMainWindow
{
    Q_OBJECT

    public:
    explicit EditTeams(QWidget *parent = 0);
    void setCon(ManageTeam*, Team&);
    ~EditTeams();

    private slots:
    void on_updateTeam_clicked();
    void on_back_button_clicked();

    private:
    Ui::EditTeams *ui;
    ManageTeam *mt;
    Team t;

    signals:
    void finished(int);
};

#endif // EDITTEAMS_H
