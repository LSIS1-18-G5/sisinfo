#include "plus_matches.h"
#include "ui_plus_matches.h"
#include "start_matches.h"
#include "trial_matches.h"
#include "view_matches.h"
#include "menumain.h"
#include <QtGui/QStandardItemModel>
#include <QtGui/QStandardItem>
#include <QtCore/QString>
#include <QtWidgets/QMessageBox>

PlusMatches::PlusMatches(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PlusMatches)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    mm = new ManageMatch();

    QPixmap pixBotton1("app/views/img/+.png");
    ui->startMatches->setIcon(pixBotton1);

    QPixmap pixBotton2("app/views/img/reload.png");
    ui->reloadMatches->setIcon(pixBotton2);

    QPixmap pixBotton3("app/views/img/view.png");
    ui->viewMatches->setIcon(pixBotton3);

    QPixmap pixBotton4("app/views/img/minus.png");
    ui->deleteMatches->setIcon(pixBotton4);

    QPixmap pixBotton5("app/views/img/search.png");
    ui->searchMatches->setIcon(pixBotton5);

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);
    
    populateUI();

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->lineEdit->setPlaceholderText("Robot...");
}

PlusMatches::~PlusMatches()
{
    delete mm;
    delete ui;
}

void PlusMatches::populateUI()
{
    QList<Match> matches = mm->listMatches();
    this->populateTableView(matches);
}

void PlusMatches::populateTableView(QList<Match> matches)
{
    QStandardItemModel *pmodel = new QStandardItemModel(this);

    for(int i = 0; i<matches.count(); i++)
    {
        pmodel->setItem(i,0,new QStandardItem(QString::number(matches[i].getUID())));
        pmodel->setItem(i,1,new QStandardItem(QString::fromStdString(mm->getRobotById(matches[i].getRobot()))));
        pmodel->setItem(i,2,new QStandardItem(QString::fromStdString(matches[i].getTimeStart())));
        pmodel->setItem(i,3,new QStandardItem(QString::fromStdString(matches[i].getTimeFinish())));
    }

    pmodel->setHeaderData(0, Qt::Horizontal, QObject::tr("Prova #"));
    pmodel->setHeaderData(1, Qt::Horizontal, QObject::tr("Robot"));
    pmodel->setHeaderData(2, Qt::Horizontal, QObject::tr("Inicio"));
    pmodel->setHeaderData(3, Qt::Horizontal, QObject::tr("Fim"));

    ui->tableView->setModel( pmodel );
}

void PlusMatches::on_searchMatches_clicked()
{
    QList<Match> matches = mm->searchMatches(ui->lineEdit->text().toStdString());
    populateTableView(matches);
}

void PlusMatches::on_startMatches_clicked()
{
    StartMatches *start = new StartMatches(this);
    start->setCon(mm);
    start->show();
}

void PlusMatches::on_viewMatches_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        Match m;
        m.setUID(index.sibling(row, 0).data().toInt());
        m.setRobot(mm->getRobotByName(index.sibling(row, 1).data().toString().toStdString()));
        m.setTimeStart(index.sibling(row, 2).data().toString().toStdString());
        m.setTimeFinish(index.sibling(row, 3).data().toString().toStdString());
        ViewMatches *view = new ViewMatches(this);
        view->setCon(mm, m);
        view->show();
    } else {
        QMessageBox::warning(this, "Aviso", "Tem de selecionar a prova que pretende visualizar.");
    }
}

void PlusMatches::on_deleteMatches_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();

    if(ui->tableView->selectionModel()->isSelected(index)) {
        int row = index.row();
        int reply = QMessageBox::question(this,"Eliminar Prova", "Pretende eliminar a prova #" + index.sibling(row, 0).data().toString() + " (Robot: " + index.sibling(row, 1).data().toString() + ") ?\n Aviso: Esta ação é irreversivel!", QMessageBox::Yes, QMessageBox::No);
        if(reply == QMessageBox::Yes){
            mm->deleteMatch(index.sibling(row, 0).data().toInt());
            this->populateUI();
        }
    } else {
        QMessageBox::warning(this,"Aviso", "Tem de selecionar a prova que pretende eliminar.");
    }
}

void PlusMatches::on_reloadMatches_clicked()
{
    this->populateUI();
}

void PlusMatches::on_backButton_clicked()
{
    delete mm;
    this->close();
    MenuMain *criar = new MenuMain(this);
    criar->show();
}

void PlusMatches::startTrial(int robot)
{
    TrialMatches *trial = new TrialMatches(this);
    trial->props(mm, robot);
    trial->show();
}
