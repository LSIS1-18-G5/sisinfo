#include "create_robots.h"
#include "ui_create_robots.h"
#include "menumain.h"
#include "iostream"
#include "../controllers/manage_team.h"
#include <QtWidgets/QMessageBox>

CreateRobots::CreateRobots(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CreateRobots)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(800, 600));

    QPixmap pix("app/views/img/Cute Robot Wallpaper (2).jpeg");
    ui->label_10->setPixmap(pix);
    ManageTeam *mt = new ManageTeam();
    ui->team->addItems(mt->getTeams());
    delete mt;
    
    connect(this, SIGNAL( finished(int) ), parent, SLOT( populateUI() ) );
}

void CreateRobots::setCon(ManageRobot *mr)
{
    this->mr = mr;
}

CreateRobots::~CreateRobots()
{
    delete mr;
    delete ui;
}

void CreateRobots::on_back_clicked()
{
    this->close();
}

void CreateRobots::on_inserir_clicked()
{
    if(ui->name->text().isEmpty() || ui->velocity->text().isEmpty() || ui->team->currentText().isEmpty() || 
    ui->sensors->text().isEmpty() || ui->extinguisher->text().isEmpty()){
        QMessageBox::warning(this,"Aviso","Todos os campos têm preenchimento obrigatório.");
    } else {
        Robot r;
        r.setName(ui->name->text().toStdString());
        r.setVelocity(ui->velocity->text().toDouble());
        r.setSensors(ui->sensors->text().toInt());
        r.setExtinguisher(ui->extinguisher->text().toStdString());
        r.setTeam((mr)->getTeamByName(ui->team->currentText().toStdString()));

        int result = mr->createRobot(r);
        if(result == 1) {
            QMessageBox::information(this,"Sucesso","Robot inserido com sucesso!");
            std::cout << "Robot inserido com sucesso!" <<std::endl;
            emit finished(1);
            this->close();
        } else {
            std::cout << "Erro ao inserir!" << std::endl;
        }
    }
}
