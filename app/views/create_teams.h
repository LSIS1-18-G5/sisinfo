#ifndef CREATETEAMS_H
#define CREATETEAMS_H

#include <QtWidgets/QMainWindow>
#include "plus_teams.h"

namespace Ui 
{
    class CreateTeams;
}

class CreateTeams : public QMainWindow
{
    Q_OBJECT

    public:
    explicit CreateTeams(QWidget *parent = 0);
    void setCon(ManageTeam*);
    ~CreateTeams();

    private slots:
    void on_back_button_clicked();
    void on_inserir_clicked();

    private:
    Ui::CreateTeams *ui;
    ManageTeam * mt;

    signals:
    void finished(int);
};

#endif // CREATETEAMS_H
