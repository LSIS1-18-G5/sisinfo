#include "team.h"

Team::Team(){
    uid = 0;
    name = "";
    color = "";
    robots = 0;
    members = 0;
}

Team::Team(int uid, string name, string color, int robots, int members){
    this->uid = uid;
    this->name = name;
    this->color = color;
    this->robots = robots;
    this->members = members;
}

/* GET */

int Team::getUID() {
    return uid;
}
    
string Team::getName() {
    return name;
}

string Team::getColor() {
    return color;
}

int Team::getRobots() {
    return robots;
}

int Team::getMembers() {
    return members;
}

/* SET */

void Team::setUID(int uid) {
    this->uid = uid;
}

void Team::setName(string name) {
    this->name = name;
}

void Team::setColor(string color) {
    this->color = color;
}

void Team::setRobots(int robots) {
    this->robots = robots;
}

void Team::setMembers(int members) {
    this->members = members;
}