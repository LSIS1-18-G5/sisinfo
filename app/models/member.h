#include <string>

using namespace std;

class Member{

    int uid;
    string name;
    int number;
	int team_id;

    public:

    Member();
    Member(int, string, int, int);
    
    int getUID();
    string getName();
    int getNumber();
    int getTeam();

    void setUID(int);
    void setName(string);
    void setNumber(int);
    void setTeam(int);
};