#include "robot.h"

Robot::Robot() {
    uid = 0;
    name = "";
    velocity = 0.0;
    sensors = 0;
    extinguisher = "";
    detected = 0;
    total_distance = 0;
    team_id = 0;
    map_id = 0;
}

Robot::Robot(int uid, string name, float velocity, int sensors, string extinguisher, int detected, int total_distance, int team_id, int map_id) {
    this->uid = uid;
    this->name = name;
    this->velocity = velocity;
    this->sensors = sensors;
    this->extinguisher = extinguisher;
    this->detected = detected;
    this->total_distance = total_distance;
    this->team_id = team_id;
    this->map_id = map_id;
}

/* GET */

int Robot::getUID() {
    return uid;
}

string Robot::getName() {
    return name;
}

float Robot::getVelocity() {
    return velocity;
}

int Robot::getSensors() {
    return sensors;
}

string Robot::getExtinguisher() {
    return extinguisher;
}

int Robot::getDetected() {
    return detected;
}

int Robot::getTotalDistance() {
    return total_distance;
}

int Robot::getTeam() {
    return team_id;
}

int Robot::getMap() {
    return map_id;
}

/* SET */

void Robot::setUID(int uid) {
    this->uid = uid;
}

void Robot::setName(string name) {
    this->name = name;
}

void Robot::setVelocity(float velocity) {
    this->velocity = velocity;
}

void Robot::setSensors(int sensors) {
    this->sensors = sensors;
}

void Robot::setExtinguisher(string extinguisher) {
    this->extinguisher = extinguisher;
}

void Robot::setDetected(int detected) {
    this->detected = detected;
}

void Robot::setTotalDistance(int total_distance) {
    this->total_distance = total_distance;
}

void Robot::setTeam(int team_id) {
    this->team_id = team_id;
}

void Robot::setMap(int map_id) {
    this->map_id = map_id;
}