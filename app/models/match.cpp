#include "match.h"

Match::Match() 
{
    uid = 0;
    robot_id = 0;
    time_start = "";
    time_finish = "";
}

Match::Match(int uid, int robot_id, string time_start, string time_finish) 
{
    this->uid = uid;
    this->robot_id = robot_id;
    this->time_start = time_start;
    this->time_finish = time_finish;
}

/* GET */

int Match::getUID() 
{
    return uid;
}
    
int Match::getRobot() 
{
    return robot_id;
}

string Match::getTimeStart() 
{
    return time_start;
}

string Match::getTimeFinish() 
{
    return time_finish;
}

/* SET */

void Match::setUID(int uid) 
{
    this->uid = uid;
}

void Match::setRobot(int robot_id) 
{
    this->robot_id = robot_id;
}

void Match::setTimeStart(string time_start) 
{
    this->time_start = time_start;
}

void Match::setTimeFinish(string time_finish) 
{
    this->time_finish = time_finish;
}