#include <string>

using namespace std;

class Trial
{
    int uid;
    int match_id;
    int state;
	string timestamp;

    public:

    Trial();
    Trial(int, int, int, string);
    
    int getUID();
    int getMatch();
    int getState();
    string getTimestamp();

    void setUID(int);
    void setMatch(int);
    void setState(int);
    void setTimestamp(string);
};