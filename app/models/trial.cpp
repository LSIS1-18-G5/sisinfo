#include "trial.h"

Trial::Trial() 
{
    uid = 0;
    match_id = 0;
    state = 0;
    timestamp = "";
}

Trial::Trial(int uid, int match_id, int state, string timestamp) 
{
    this->uid = uid;
    this->match_id = match_id;
    this->state = state;
    this->timestamp = timestamp;
}

/* GET */

int Trial::getUID() 
{
    return uid;
}
    
int Trial::getMatch() 
{
    return match_id;
}

int Trial::getState() 
{
    return state;
}

string Trial::getTimestamp() 
{
    return timestamp;
}

/* SET */

void Trial::setUID(int uid) 
{
    this->uid = uid;
}

void Trial::setMatch(int match_id) 
{
    this->match_id = match_id;
}

void Trial::setState(int state) 
{
    this->state = state;
}

void Trial::setTimestamp(string timestamp) 
{
    this->timestamp = timestamp;
}