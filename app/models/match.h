#include <string>

using namespace std;

class Match
{
    int uid;
    int robot_id;
    string time_start;
	string time_finish;

    public:

    Match();
    Match(int, int, string, string);
    
    int getUID();
    int getRobot();
    string getTimeStart();
    string getTimeFinish();

    void setUID(int);
    void setRobot(int);
    void setTimeStart(string);
    void setTimeFinish(string);
};