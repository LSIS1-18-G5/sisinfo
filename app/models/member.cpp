#include "member.h"

Member::Member() {
    uid = 0;
    name = "";
    number = 0;
    team_id = 0;
}

Member::Member(int uid, string name, int number, int team_id) {
    this->uid = uid;
    this->name = name;
    this->number = number;
    this->team_id = team_id;
}

/* GET */

int Member::getUID() {
    return uid;
}
    
string Member::getName() {
    return name;
}

int Member::getNumber() {
    return number;
}

int Member::getTeam() {
    return team_id;
}

/* SET */

void Member::setUID(int uid) {
    this->uid = uid;
}

void Member::setName(string name) {
    this->name = name;
}

void Member::setNumber(int number) {
    this->number = number;
}

void Member::setTeam(int team_id) {
    this->team_id = team_id;
}