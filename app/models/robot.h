#include <string>

using namespace std;

class Robot{

    int uid;
    string name;
    float velocity;
    int sensors;
	string extinguisher;
	int detected;
	int total_distance;
	int team_id;
	int map_id;

    public:

    Robot();
    Robot(int, string, float, int, string, int, int, int, int);
    
    int getUID();
    string getName();
    float getVelocity();
    int getSensors();
    string getExtinguisher();
    int getDetected();
    int getTotalDistance();
    int getTeam();
    int getMap();

    void setUID(int);
    void setName(string);
    void setVelocity(float);
    void setSensors(int);
    void setExtinguisher(string);
    void setDetected(int);
    void setTotalDistance(int);
    void setTeam(int);
    void setMap(int);
};