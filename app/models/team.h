#include <string>

using namespace std;

class Team{

    int uid;
    string name;
	string color;
	int robots;
	int members;

    public:

    Team();
    Team(int, string, string, int, int);
    
    int getUID();
    string getName();
    string getColor();
    int getRobots();
    int getMembers();

    void setUID(int);
    void setName(string);
    void setColor(string);
    void setRobots(int);
    void setMembers(int);
};