#include "../models/team.h"
#include "database.h"
#include <QtCore/QStringList>
#include <QtCore/QList>

class ManageTeam
{
    Driver *driver;
    ResultSet *res;
    Connection *con;

    public:
    ManageTeam();

    QList<Team> listTeams();
    QList<Team> searchTeams(string);
    QStringList getTeams();
    int createTeam(Team&);
    //viewTeam();
    int updateTeam(Team&);
    int deleteTeam(int);

    void closeConnection();
    ~ManageTeam();
};