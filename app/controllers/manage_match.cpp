#include "manage_match.h"

ManageMatch::ManageMatch()
{
    try {
		driver = get_driver_instance();
		con = driver->connect(DBHOST, USER, PASSWORD);
        con->setSchema(DBASE);
		cout << "Connected to database 'lsis' by 'match'." << endl;
	} catch (SQLException &e) {
  		cout << "# ERR: SQLException in " << __FILE__;
  		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  		cout << "# ERR: " << e.what();
  		cout << " (MySQL error code: " << e.getErrorCode();
  		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

QList<Match> ManageMatch::listMatches()
{
    QList<Match> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM matches");
    
    try {
        res = ps->executeQuery();
        
        while(res->next()){
            Match m;
            m.setUID(res->getInt("id"));
            m.setRobot(res->getInt("robot_id"));
            m.setTimeStart(res->getString("time_start"));
            m.setTimeFinish(res->getString("time_finish"));
            result.append(m);    
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    ps->close();
    delete ps;
    return result;
}

QList<Match> ManageMatch::searchMatches(string nome)
{
    QList<Match> result;
    PreparedStatement *ps = con->prepareStatement("SELECT matches.id, matches.robot_id, matches.time_start, matches.time_finish FROM matches RIGHT JOIN robots ON matches.robot_id = robots.id WHERE robots.name LIKE CONCAT('%', ?, '%') ORDER BY matches.id");
    
    try {
        ps->setString(1,nome);
        res = ps->executeQuery();
        
        while(res->next()) {
            Match m;
            m.setUID(res->getInt("id"));
            m.setRobot(res->getInt("robot_id"));
            m.setTimeStart(res->getString("time_start"));
            m.setTimeFinish(res->getString("time_finish"));
            result.append(m);   
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

QStringList ManageMatch::getRobots()
{
    QStringList result;
    PreparedStatement *ps = con->prepareStatement("SELECT name FROM robots");
    
    try {
        res = ps->executeQuery();
        
        while(res->next()) {
            result << QString::fromStdString(res->getString("name"));
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

int ManageMatch::deleteMatch(int id)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("DELETE FROM matches WHERE id=?");
    
    try {
        ps->setInt(1, id);
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

int ManageMatch::startTrial(int id)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("INSERT INTO matches (robot_id) VALUES (?)");
    
    try {
        ps->setInt(1, id);
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

void ManageMatch::saveTrial(int match_id, int state)
{
    PreparedStatement *ps = con->prepareStatement("INSERT INTO trials (match_id, state) VALUES (?, ?)");
    
    try {
        ps->setInt(1, match_id);
        ps->setInt(2, state);
        ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
}

int ManageMatch::stopTrial(int id)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("UPDATE matches SET time_finish=NOW() WHERE id=?");
    
    try {
        ps->setInt(1, id);
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}
#include "../controllers/manage_robot.h"
string ManageMatch::getRobotById(int id)
{
    string result = "";
    PreparedStatement *ps = con->prepareStatement("SELECT name FROM robots WHERE id=? LIMIT 1");
    
    try {
        ps->setInt(1, id);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getString("name");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

int ManageMatch::getRobotByName(string team)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("SELECT id FROM robots WHERE name=? LIMIT 1");
    
    try {
        ps->setString(1, team);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getInt("id");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

string ManageMatch::getRobotTeam(int robot)
{
    string result = "";
    PreparedStatement *ps = con->prepareStatement("SELECT teams.name FROM teams, robots, matches WHERE teams.id = robots.team_id AND robots.id = matches.robot_id AND robots.id = ? LIMIT 1");

    try {
        ps->setInt(1, robot);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getString("name");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

int ManageMatch::getLastId()
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("SELECT id FROM matches ORDER BY id DESC LIMIT 1");
    
    try {
        res = ps->executeQuery();
        while(res->next()){
            result = res->getInt("id");
        }
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

QList<Trial> ManageMatch::getTrials(int match)
{
    QList<Trial> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM trials WHERE match_id = ?");
    
    try {
        ps->setInt(1, match);
        res = ps->executeQuery();

        while(res->next()) {
            Trial t;
            t.setUID(res->getInt("id"));
            t.setMatch(res->getInt("match_id"));
            t.setState(res->getInt("state"));
            t.setTimestamp(res->getString("timestamp"));
            result.append(t);
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

void ManageMatch::fireDetected(int id)
{
    PreparedStatement *ps = con->prepareStatement("UPDATE robots SET detected = detected + 1 WHERE robots.id = ?");
    
    try {
        ps->setInt(1, id);
        ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
}

void ManageMatch::closeConnection() 
{
    con->close();
}

ManageMatch::~ManageMatch() 
{
    delete con;
    cout << "Disconnected from database 'lsis' by 'match'." << endl;
}
