#include "../models/robot.h"
#include "database.h"
#include <QtCore/QList>

class ManageRobot
{
    Driver *driver;
    Connection *con;
    ResultSet *res;

    public:
    ManageRobot();

    QList<Robot> listRobots();
    QList<Robot> searchRobots(string);
    int createRobot(Robot&);
    int updateRobot(Robot&, int);
    int deleteRobot(int);

    string getTeamById(int);
    int getTeamByName(string);
    void addToTeam(int);
    void removeFromTeam(int);

    void closeConnection();
    ~ManageRobot();
};