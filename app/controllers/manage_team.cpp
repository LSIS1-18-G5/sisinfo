#include "manage_team.h"
#include <QtCore/QString>

ManageTeam::ManageTeam() {
    try{
		driver = get_driver_instance();
		con = driver->connect(DBHOST, USER, PASSWORD);
        con->setSchema(DBASE);
		cout << "Connected to database 'lsis' by 'team'." << endl;
	} catch (SQLException &e) {
  		cout << "# ERR: SQLException in " << __FILE__;
  		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  		cout << "# ERR: " << e.what();
  		cout << " (MySQL error code: " << e.getErrorCode();
  		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

QList<Team> ManageTeam::listTeams()
{
    QList<Team> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM teams");
    
    try {
        res = ps->executeQuery();
        
        while(res->next()){
            Team t;
            t.setUID(res->getInt("id"));
            t.setName(res->getString("name"));
            t.setColor(res->getString("color"));
            t.setRobots(res->getInt("robots"));
            t.setMembers(res->getInt("members"));
            result.append(t);
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

QList<Team> ManageTeam::searchTeams(string name)
{
    QList<Team> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM `teams` WHERE `name` LIKE CONCAT('%', ?, '%')");
    
    try {
        ps->setString(1, name);
        res = ps->executeQuery();
        
        while(res->next()) {
            Team t;
            t.setUID(res->getInt("id"));
            t.setName(res->getString("name"));
            t.setColor(res->getString("color"));
            t.setRobots(res->getInt("robots"));
            t.setMembers(res->getInt("members"));
            result.append(t);  
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

QStringList ManageTeam::getTeams()
{
    QStringList result;
    PreparedStatement *ps = con->prepareStatement("SELECT name FROM teams");
    
    try {
        res = ps->executeQuery();
        
        while(res->next()) {
            result << QString::fromStdString(res->getString("name"));
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

int ManageTeam::createTeam(Team& t) 
{
    int result=0;
    PreparedStatement *ps = con->prepareStatement("INSERT INTO teams(name, color, robots, members) VALUES (?,?,0,0)");
    try {
        ps->setString(1, t.getName());
        ps->setString(2, t.getColor());
        result= ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
      cout<<"Exception occurred"<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

//viewTeam();

int ManageTeam::updateTeam(Team& t)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("UPDATE teams SET name=?, color=?, robots=?, members=? WHERE id=?");
    
    try {
        ps->setString(1, t.getName());
        ps->setString(2, t.getColor());
        ps->setInt(3, t.getRobots());
        ps->setInt(4, t.getMembers());
        ps->setInt(5, t.getUID());
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

int ManageTeam::deleteTeam(int id)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("DELETE FROM teams WHERE id=?");
    
    try {
        ps->setInt(1, id);
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

void ManageTeam::closeConnection() 
{
    con->close();
}

ManageTeam::~ManageTeam() 
{
    delete con;
    cout << "Disconnected from database 'lsis' by 'team'." << endl;
}
