#include "../models/member.h"
#include "database.h"
#include <QtCore/QList>

class ManageMember
{
    Driver *driver;
    Connection *con;
    ResultSet *res;

    public:
    ManageMember();

    QList<Member> listMembers();
    QList<Member> searchMembers(string);
    int createMember(Member&);
    int updateMember(Member&, int);
    int deleteMember(int);

    string getTeamById(int);
    int getTeamByName(string);
    void addToTeam(int);
    void removeFromTeam(int);

    void closeConnection();
    ~ManageMember();
};