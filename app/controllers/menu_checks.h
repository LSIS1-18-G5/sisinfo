#include "database.h"

class MenuChecks
{
    Driver *driver;
    ResultSet *res;
    Connection *con;

    public:
    MenuChecks();

    int checkElement(string);

    void closeConnection();
    ~MenuChecks();
};