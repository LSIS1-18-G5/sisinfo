#include "manage_robot.h"

ManageRobot::ManageRobot()
{
    try {
		driver = get_driver_instance();
		con = driver->connect(DBHOST, USER, PASSWORD);
        con->setSchema(DBASE);
		cout << "Connected to database 'lsis' by 'robot'." << endl;
	} catch (SQLException &e) {
  		cout << "# ERR: SQLException in " << __FILE__;
  		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  		cout << "# ERR: " << e.what();
  		cout << " (MySQL error code: " << e.getErrorCode();
  		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

QList<Robot> ManageRobot::listRobots()
{
    QList<Robot> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM robots");
    
    try {
        res = ps->executeQuery();
        
        while(res->next()) {
            Robot r;
            r.setUID(res->getInt("id"));
            r.setName(res->getString("name"));
            r.setVelocity(res->getDouble("velocity"));
            r.setSensors(res->getInt("sensors"));
            r.setExtinguisher(res->getString("extinguisher"));
            r.setDetected(res->getInt("detected"));
            r.setTotalDistance(res->getInt("total_distance"));
            r.setTeam(res->getInt("team_id"));
            result.append(r);
        }

    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

QList<Robot> ManageRobot::searchRobots(string nome)
{
    QList<Robot> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM `robots` WHERE `name` LIKE CONCAT('%', ?, '%')");
    
    try {
        ps->setString(1,nome);
        res = ps->executeQuery();
        
        while(res->next()) {
            Robot r;
            r.setUID(res->getInt("id"));
            r.setName(res->getString("name"));
            r.setVelocity(res->getDouble("velocity"));
            r.setSensors(res->getInt("sensors"));
            r.setExtinguisher(res->getString("extinguisher"));
            r.setDetected(res->getInt("detected"));
            r.setTotalDistance(res->getInt("total_distance"));
            r.setTeam(res->getInt("team_id"));
            result.append(r);  
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

int ManageRobot::createRobot(Robot& r)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("INSERT INTO robots (name, velocity, sensors, extinguisher, detected, total_distance, team_id) VALUES (?,?,?,?,0,0,?)");
    
    try {
        ps->setString(1, r.getName());
        ps->setDouble(2, r.getVelocity());
        ps->setInt(3, r.getSensors());
        ps->setString(4, r.getExtinguisher());
        ps->setInt(5, r.getTeam());
        result = ps->executeUpdate();
        con->commit();
        addToTeam(r.getTeam());
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

int ManageRobot::updateRobot(Robot& r, int old_team)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("UPDATE robots SET name=?, velocity=?, sensors=?, extinguisher=?, detected=?, total_distance=?, team_id=? WHERE id=?");
    
    try {
        ps->setString(1, r.getName());
        ps->setDouble(2, r.getVelocity());
        ps->setInt(3, r.getSensors());
        ps->setString(4, r.getExtinguisher());
        ps->setInt(5, r.getDetected());
        ps->setInt(6, r.getTotalDistance());
        ps->setInt(7, r.getTeam());
        ps->setInt(8, r.getUID());
        result = ps->executeUpdate();
        removeFromTeam(old_team);
        addToTeam(r.getTeam());
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

int ManageRobot::deleteRobot(int id)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("DELETE FROM robots WHERE id=?");
    
    try {
        ps->setInt(1, id);
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

string ManageRobot::getTeamById(int id)
{
    string result = "";
    PreparedStatement *ps = con->prepareStatement("SELECT name FROM teams WHERE id=? LIMIT 1");
    
    try {
        ps->setInt(1, id);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getString("name");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

int ManageRobot::getTeamByName(string team)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("SELECT id FROM teams WHERE name=? LIMIT 1");
    
    try {
        ps->setString(1, team);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getInt("id");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

void ManageRobot::addToTeam(int team)
{
    PreparedStatement *ps = con->prepareStatement("UPDATE teams SET robots = robots + 1 WHERE id = ?");
    
    try {
        ps->setInt(1, team);
        ps->executeUpdate();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
}

void ManageRobot::removeFromTeam(int team)
{
    PreparedStatement *ps = con->prepareStatement("UPDATE teams SET robots = robots - 1 WHERE id = ?");
    
    try {
        ps->setInt(1, team);
        ps->executeUpdate();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
}

void ManageRobot::closeConnection() 
{
    con->close();
}

ManageRobot::~ManageRobot() 
{
    delete con;
    cout << "Disconnected from database 'lsis' by 'robot'." << endl;
}