#include "manage_member.h"

ManageMember::ManageMember()
{
    try {
		driver = get_driver_instance();
		con = driver->connect(DBHOST, USER, PASSWORD);
        con->setSchema(DBASE);
		cout << "Connected to database 'lsis' by 'member'." << endl;
	} catch (SQLException &e) {
  		cout << "# ERR: SQLException in " << __FILE__;
  		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  		cout << "# ERR: " << e.what();
  		cout << " (MySQL error code: " << e.getErrorCode();
  		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

QList<Member> ManageMember::listMembers()
{
    QList<Member> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM members");
    
    try {
        res = ps->executeQuery();
        
        while(res->next()){
            Member m;
            m.setUID(res->getInt("id"));
            m.setName(res->getString("name"));
            m.setNumber(res->getInt("number"));
            m.setTeam(res->getInt("team_id"));
            result.append(m);    
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    ps->close();
    delete ps;
    return result;
}

QList<Member> ManageMember::searchMembers(string nome)
{
    QList<Member> result;
    PreparedStatement *ps = con->prepareStatement("SELECT * FROM `members` WHERE `name` LIKE CONCAT('%', ?, '%')");
    
    try {
        ps->setString(1,nome);
        res = ps->executeQuery();
        
        while(res->next()) {
            Member m;
            m.setUID(res->getInt("id"));
            m.setName(res->getString("name"));
            m.setNumber(res->getInt("number"));
            m.setTeam(res->getInt("team_id"));
            result.append(m);   
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

int ManageMember::createMember(Member& m)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("INSERT INTO members (name, number, team_id) VALUES (?,?,?)");
    try {
        ps->setString(1, m.getName());
        ps->setInt(2, m.getNumber());
        ps->setInt(3, m.getTeam());
        result = ps->executeUpdate();
        con->commit();
        addToTeam(m.getTeam());
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

int ManageMember::updateMember(Member& m, int old_team)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("UPDATE members SET name=?, number=?, team_id=? WHERE id=?");
    
    try {
        ps->setString(1, m.getName());
        ps->setInt(2, m.getNumber());
        ps->setInt(3, m.getTeam());
        ps->setInt(4, m.getUID());
        result = ps->executeUpdate();
        con->commit();
        removeFromTeam(old_team);
        addToTeam(m.getTeam());
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
   
    ps->close();
    delete ps;
    return result;
}

int ManageMember::deleteMember(int id)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("DELETE FROM members WHERE id=?");
    
    try {
        ps->setInt(1, id);
        result = ps->executeUpdate();
        con->commit();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }

    ps->close();
    delete ps;
    return result;
}

string ManageMember::getTeamById(int id)
{
    string result = "";
    PreparedStatement *ps = con->prepareStatement("SELECT name FROM teams WHERE id=? LIMIT 1");
    
    try {
        ps->setInt(1, id);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getString("name");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

int ManageMember::getTeamByName(string team)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("SELECT id FROM teams WHERE name=? LIMIT 1");
    
    try {
        ps->setString(1, team);
        res = ps->executeQuery();
        while(res->next()) {
            result = res->getInt("id");
        }
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

void ManageMember::addToTeam(int team)
{
    PreparedStatement *ps = con->prepareStatement("UPDATE teams SET members = members + 1 WHERE id = ?");
    
    try {
        ps->setInt(1, team);
        ps->executeUpdate();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
}

void ManageMember::removeFromTeam(int team)
{
    PreparedStatement *ps = con->prepareStatement("UPDATE teams SET members = members - 1 WHERE id = ?");
    
    try {
        ps->setInt(1, team);
        ps->executeUpdate();
    } catch(SQLException &ex) {
        cout<<"Exception occurred: "<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
}

void ManageMember::closeConnection() 
{
    con->close();
}

ManageMember::~ManageMember() 
{
    delete con;
    cout << "Disconnected from database 'lsis' by 'member'." << endl;
}