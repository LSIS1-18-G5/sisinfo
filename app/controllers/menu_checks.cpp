#include "menu_checks.h"

MenuChecks::MenuChecks() {
    try{
		driver = get_driver_instance();
		con = driver->connect(DBHOST, USER, PASSWORD);
        con->setSchema(DBASE);
		cout << "Connected to database 'lsis' by 'menu'." << endl;
	} catch (SQLException &e) {
  		cout << "# ERR: SQLException in " << __FILE__;
  		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  		cout << "# ERR: " << e.what();
  		cout << " (MySQL error code: " << e.getErrorCode();
  		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

int MenuChecks::checkElement(string table)
{
    int result = 0;
    PreparedStatement *ps = con->prepareStatement("SELECT COUNT(id) FROM " + table);
    try {
        res = ps->executeQuery();
        while(res->next()){
            result = res->getInt(1);
        }
    } catch(SQLException &ex) {
      cout<<"Exception occurred"<<ex.getErrorCode()<<endl;
    }
    
    ps->close();
    delete ps;
    return result;
}

void MenuChecks::closeConnection() 
{
    con->close();
}

MenuChecks::~MenuChecks() 
{
    delete con;
    cout << "Disconnected from database 'lsis' by 'menu'." << endl;
}