#include "../models/match.h"
#include "../models/trial.h"
#include "database.h"
#include <QtCore/QStringList>
#include <QtCore/QList>

class ManageMatch
{
    Driver *driver;
    Connection *con;
    ResultSet *res;

    public:
    ManageMatch();

    QList<Match> listMatches();
    QList<Match> searchMatches(string);
    QStringList getRobots();
    int deleteMatch(int);

    int startTrial(int);
    void saveTrial(int, int);
    int stopTrial(int);
    string getRobotById(int);
    int getRobotByName(string);
    string getRobotTeam(int);
    int getLastId();
    QList<Trial> getTrials(int);
    void fireDetected(int);

    void closeConnection();
    ~ManageMatch();
};